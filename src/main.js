import './public-path' // 设置webpack的public-path要在elementUI等资源之前引入，不然获取不到资源
import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import router from './router.js'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Vuex from 'vuex'
import VueCookies from 'vue-cookies'
import CryptoJS from 'crypto-js'
// import COS from 'cos-js-sdk-v5'
// var COS = require('cos-js-sdk-v5')
// import * as qiniu from 'qiniu-js'
// import NodeRSA from 'node-rsa'

Vue.use(VueRouter)
Vue.use(ElementUI)
Vue.use(VueAxios, axios)
Vue.use(Vuex)
Vue.use(VueCookies)
Vue.use(CryptoJS)

Vue.config.productionTip = false

Vue.prototype.$axios = axios.create({
  baseURL: 'http://101.33.205.30:3000/'
})
let myAxios = axios.create({
  baseURL: 'http://101.33.205.30:3000/'
})
Vue.prototype.$crypto = CryptoJS
// Vue.prototype.$qiniu = qiniu
Vue.prototype.$cookies = VueCookies
Vue.$cookies.config('3d')

// vuex 状态管理
let store = new Vuex.Store({
  state: {
    qk_router_prefix: '',
    loginFlag: false,
    userInfo: {
      userId: '',
      username: '',
      description: '',
      submittedAtc: [],
      savedAtc: [],
      deletedAtc: []
    },
    atcList: [],
    operateAtc: {},
    deleteFlag: false
  },
  mutations: {
    // qiankun框架下运行设置路由前缀
    setRouterPrefix(state, pre) {
      state.qk_router_prefix = pre
    },
    // 登录
    login(state, info) {
      state.loginFlag = true
      state.userInfo.userId = info.userId
      state.userInfo.username = info.username
      state.userInfo.description = info.description
    },
    // 退出登录
    logout(state) {
      state.loginFlag = false
      state.userInfo.userId = ''
      state.userInfo.username = ''
      state.userInfo.description = ''
    },
    // 首页 请求所有 已发表 文章信息
    setArticleList(state, list) {
      state.atcList = list
    },
    // 修改个人信息
    updatePersonalInfo(state, info) {
      state.userInfo.description = info.description
    },
    // 要操作的文章的 文章信息
    setOperateAtc(state, info) {
      state.operateAtc = info
    },
    // 完成删除文章的标志，用于刷新 我的博客 组件的数据
    setDeleteFlag(state, flag) {
      state.deleteFlag = flag
    }
  },
})

const validLogin = ()=> {
  // cookies验证登录
  if (Vue.$cookies.get('user_id') && Vue.$cookies.get('user_pw')) {
    let id = parseInt(Vue.$cookies.get('user_id'))
    let pw = Vue.$cookies.get('user_pw')
    myAxios
      .post("/login/validateLogin", {
        userId: id,
        password: pw
      })
      .then(ret => {
        if (ret.data.length === 1) {
          // cookies验证登陆成功，将用户信息存到vuex
          let info = {
            userId: ret.data[0].user_id,
            username: ret.data[0].username,
            description: ret.data[0].description
          };
          store.commit("login", info);
        } else {
          // 验证登录失败，跳转到登录页面
          console.log("cookies验证登录失败");
        }
      });
  }
}

// 全局前置守卫
router.beforeEach((to, from, next) => {
  // 如果页面需要登录
  if (to.meta.requireLogin) {
    // 由于新标签页无法获取这里的vuex的数据，所以要向数据库验证
    // cookies验证登录
    if (Vue.$cookies.get('user_id') && Vue.$cookies.get('user_pw')) {
      let id = parseInt(Vue.$cookies.get('user_id'))
      let pw = Vue.$cookies.get('user_pw')
      myAxios
        .post("/login/validateLogin", {
          userId: id,
          password: pw
        })
        .then(ret => {
          if (ret.data.length === 1) {
            // cookies验证登陆成功，跳转到对应的路由
            let info = {
              userId: ret.data[0].user_id,
              username: ret.data[0].username,
              description: ret.data[0].description
            };
            store.commit("login", info);
            next()
          } else {
            // 验证登录失败，跳转到登录页面
            console.log("cookies验证登录失败,估计有扑街修改了cookie");
            next({
              path: '/loginRegister'
            })
          }
        });
    } else {
      console.log('未登录啊老哥')
      next({
        path: '/loginRegister'
      })
    }
  } else {
    next()
  }
})

// ======qiankun config begin========================
let instance = null;
// let qk_router = null;

/**
 * 渲染函数
 * 两种情况：主应用生命周期钩子中运行 / 微应用单独启动时运行
 */
function render() {
  // 在 render 中创建 VueRouter，可以保证在卸载微应用时，移除 location 事件监听，防止事件污染

  if (window.__POWERED_BY_QIANKUN__) {
    store.commit("setRouterPrefix", '/app1')
    console.log('----:', store.state.qk_router_prefix)
    router.beforeEach((to, from, next) => {
      // qiankun框架中运行时，转到app1/路径下的路由
      if (!to.path.includes('/app1')) {
        next({
          path: '/app1' + to.path
        })
      }else{
        next()
      }
    })
  }

  // 挂载应用
  instance = new Vue({
    render: h => h(App),
    router,
    store,
    created() {
      validLogin()
    }
  }).$mount('#blogSystem')
  // instance = new Vue({
  //   qk_router,
  //   render: (h) => h(App),
  // }).$mount("#vue2");
}

// 独立运行时，直接挂载应用
if (!window.__POWERED_BY_QIANKUN__) {
  render();
}

/**
 * bootstrap 只会在微应用初始化的时候调用一次，下次微应用重新进入时会直接调用 mount 钩子，不会再重复触发 bootstrap。
 * 通常我们可以在这里做一些全局变量的初始化，比如不会在 unmount 阶段被销毁的应用级别的缓存等。
 */
export async function bootstrap() {
  console.log("VueMicroApp bootstraped");
}

/**
 * 应用每次进入都会调用 mount 方法，通常我们在这里触发应用的渲染方法
 */
export async function mount(props) {
  console.log("VueMicroApp mount", props);
  render(props);
}

/**
 * 应用每次 切出/卸载 会调用的方法，通常在这里我们会卸载微应用的应用实例
 */
export async function unmount() {
  console.log("VueMicroApp unmount");
  instance.$destroy();
  instance = null;
  router = null;
}
// ======qiankun config end========================
