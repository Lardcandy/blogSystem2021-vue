import VueRouter from 'vue-router'

// 2021-04-11 写Elisabirthday组件时暂时失效
import index from './components/index.vue'
import discussArea from './components/discussArea.vue'
import resource from './components/resource.vue'
import blogEdit from './components/blogEdit.vue'
import personalCenter from './components/personalCenter.vue'
import article from './components/indexSub/article.vue'
import loginRegister from './components/loginRegister.vue'
import articleDetails from './components/articleDetails.vue'
// import admin from './components/admin.vue'
import elisaBirthday from './components/elisaBirthday.vue'
import arrayPage from './components/resource/arrayPage.vue'

let qk_router_pre = ''
// 如果是qiankun框中，因为是子应用，路由要加/app1前缀
if(window.__POWERED_BY_QIANKUN__) {
    qk_router_pre = '/app1'
}
var router = new VueRouter({
    mode: 'hash',
    routes:[
        // 首页  讨论区  资源  写博客  个人中心
        {
            path: qk_router_pre + '/', 
            component:index,
            children:[
                { path: '/', component:article}
            ],
        },
        //2021-04-11,彤彤生日，
        {
            path: qk_router_pre + '/secretGarden', 
            component:elisaBirthday,
        },
        {
            path: qk_router_pre + '/discussArea',
            component:discussArea,
        },
        {
            path: qk_router_pre + '/resource',
            component:resource,
            children: [
                {
                    path: '/arrayPage',
                    component: arrayPage
                }
            ]
        },
        {
            path: qk_router_pre + '/blogEdit',
            component:blogEdit,
            meta:{
                requireLogin: true,
            }
        },
        {
            path: qk_router_pre + '/blogEdit/:id',
            component:blogEdit,
            meta:{
                requireLogin: true,
            }
        },
        {
            path: qk_router_pre + '/personalCenter',
            component:personalCenter,
            meta:{
                requireLogin: true,
            }
        },
        {
            path: qk_router_pre + '/loginRegister',
            component:loginRegister,
        },
        {
            path: qk_router_pre + '/articleDetails/:id',
            component: articleDetails,
        }

    ]
})

export default router