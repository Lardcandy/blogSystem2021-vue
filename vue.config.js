const path = require('path');

const webpack = require('webpack')
const CompressionWebpackPlugin = require('compression-webpack-plugin')
// const ExtractTextPlugin = require('extract-text-webpack-plugin')
// const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const productionGzipExtensions = ['js', 'css']
const isProduction = process.env.NODE_ENV === 'production'
const packageName = require('./package').name

const _publicPath = isProduction?  'http://101.33.205.30:8084' : ''

module.exports = {
  devServer: {
    proxy: _publicPath,
    headers: {
        'Access-Control-Allow-Origin': '*',
    },
    // 关闭主机检查，使微应用可以被 fetch
    disableHostCheck: true,
  },
  productionSourceMap: false,
  chainWebpack: config => {
    config.module.rule('fonts').use('url-loader').tap(options => {
      options.limit = 1
      options.publicPath = _publicPath
      return options
    })
    config.module.rule('images').use('url-loader').tap(options => {
      options.limit = 1
      options.publicPath = _publicPath
      return options
    })
  },
  configureWebpack  :{
    output: {
      library: `${packageName}-[name]`,
      libraryTarget: 'umd',
      jsonpFunction: `webpackJsonp_${packageName}`,
    },
    resolve:{
      alias:{
        '@':path.resolve(__dirname, './src'),
        '@i':path.resolve(__dirname, './src/assets'),
      } 
    },
    plugins: [
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
      new CompressionWebpackPlugin({
        algorithm: 'gzip',
        test: new RegExp('\\.(' + productionGzipExtensions.join('|') + ')$'),
        threshold: 10240,
        minRatio: 0.8
      }),
      new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 5, 
        minChunkSize: 100,
      }),
    ],
    module: {
      rules: [
        // {
        //   test: /\.css$/,
        //   use: [
        //     {
        //       loader: MiniCssExtractPlugin.loader,
        //       options: {
        //         publicPath: _publicPath,
        //       },
        //     },
        //     "css-loader",
        //   ],
        // },
      ],
    },
  },
  pwa: {
    iconPaths: {
      favicon32     : 'favicon.ico',
      favicon16     : 'favicon.ico',
      appleTouchIcon: 'favicon.ico',
      maskIcon      : 'favicon.ico',
      msTileImage   : 'favicon.ico'
    }
  },
}
